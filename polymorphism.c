#include<stdio.h>
#include<stdlib.h>
typedef (*vtable)(struct Mat *m);

vtable *tb_m;
vtable *tb_v;
typedef struct Matrix {

int column;
int row;
int *array;
void (*Matrix)( struct Mat *mat, int col, int row);
void (*add)( struct Mat *mat, struct Mat *mat1, struct Mat *mat2);
void (*mult)( struct Mat *mat, struct Mat *mat1, struct Mat *mat2);
vtable *table_m;
}Mat;

typedef struct Vector{
Mat matrix;
int max;
int min;
void (*vec)( struct vector *mat, int col, int row);
vtable *table_v;
}vector;

int L1_norm(Mat *m1){
int i=0;
int sum=0;
int j=0;
int final=0;
for(j=0; j<m1->column; j++){
for(i=0; i<(m1->row); i++){
sum=sum+m1->array[j*m1->column +i];
}
if(sum>final){
final=sum;
}
}

return final;

}

int L1_norm_v(Mat *m1){
int i=0;
int sum=0;
int j=0;
int final=0;
for(i=0; i<(m1->row); i++){
sum=sum+m1->array[i];

}

return sum;

}
void Add(Mat *m1, Mat *m2 , Mat *r)
{
int *arr;
  int i,j;

r->array=(int *)malloc( (m1->row*m1->column)* sizeof(int));

for(i=0;i<(m1->column*m1->row); i++){
	r->array[i]=m1->array[i]+m2->array[i];


}

}



void Multiply(Mat *m1, Mat *m2, Mat *r){
int j=0;
int sum=0;
int i=0;
int f=0;
int l=0;

	int k = 0;

r->array=(int *)malloc( (m1->row*m2->column)* sizeof(int));
for ( i = 0; i<(m1->row*m1->column); i = i + m1->column){
		j = i;
		 f = 0;
		for (l = 0; l<(m2->row*m2->column);){
			sum = sum + (m1->array[j] * m2->array[l]);
 			l = l + m2->column;
			j++;
			if ((j) % m1->column == 0){
				r->array[k] = sum;
				j = i;
				if (l != ((m2->row*m2->column)-1) + m2->column){
				f++;
				l = f;
			}
			k++;
sum=0;
			}
		}
	
}
}
void Matrix_init( Mat *mat, int col, int row){
int **arr;
mat->add=Add;
mat->mult=Multiply;
mat->column=col;
mat->row= row;
mat->array=(int *)malloc(sizeof(int)*(row*col));
int i,j;


for(i=0;i<(col*row); i++){
mat->array[i] = i+1;
}

}
void V_Add(vector *m1, vector *m2 , vector *r)
{
int *arr;
  int i,j;
r->matrix.array=(int *)malloc( (m1->matrix.row*m1->matrix.column)* sizeof(int));

for(i=0;i<(m1->matrix.column*m1->matrix.row); i++){
	r->matrix.array[i]=m1->matrix.array[i]+m2->matrix.array[i];


}
}
void Vector_init( vector *mat, int col, int row){
int **arr;
mat->matrix.add=V_Add;
mat->matrix.mult=Multiply;
mat->matrix.column=col;
mat->matrix.row= row;
mat->matrix.array=(int *)malloc(sizeof(int)*(row*col));
int i,j;


for(i=0;i<(col*row); i++){
mat->matrix.array[i] = i+1;

}




}

int main(){
int row_m = 2; int col_m = 2; int row_v = 2; int col_v = 1;
	int input;
	
Mat m1;
Mat m2;
Mat r;
Mat r1;
vector v1;vector v2;vector v3;
int i,j;
printf("Press 1 for Vector functions, Press 2 for Matrix functions");
scanf("%d", &input);
if(input==2){


	printf("Enter rows of first matrix:\n");
scanf("%d", &row_m);
printf("Enter columns of first matrix:\n");
scanf("%d", &col_m);
printf("Enter rows of second matrix:\n");
scanf("%d", &row_v);
printf("Enter columns of second matrix:\n");
scanf("%d", &col_v);

m1.Matrix=Matrix_init;
m1.Matrix(&m1, row_m,col_m);
m2.Matrix=Matrix_init;
m2.Matrix(&m2, row_v,col_v);
	if (col_m == col_v && row_m==row_v){

m2.add(&m1, &m2,&r);
printf("Addition:\n");
for(i=0; i<m2.row;i++){
for (j=0; j<m2.column; j++){
printf("%d  ", r.array[i*m2.column +j]);
}
printf("\n");
}
}
else
printf("Addition not possible!\n");
//Matrix_init(ptr1, 2,2);
if(col_m==row_v){
m2.mult(&m1,&m2, &r1);


printf("Multiplication:\n");
for(i=0; i<row_m;i++){
for (j=0; j<col_v; j++){
printf("%d  ", r1.array[i*m2.column +j]);
}
printf("\n");
}
}
else
printf("Multiplication not possible!\n");
tb_m=(vtable *)malloc( (10)* sizeof(vtable));
tb_m[0]=L1_norm;
m1.table_m=tb_m;
m2.table_m=tb_m;
printf("%d is L1 norm of first matrix \n",m1.table_m[0](&m1));
printf("%d is L1 norm of second matrix \n",m2.table_m[0](&m2));

}
if(input==1){
printf("Enter rows of 1st vector:\n");
scanf("%d", &row_v);
printf("Enter rows of 2nd vector:\n");
scanf("%d", &col_v);
tb_v=(vtable *)malloc( (10)* sizeof(vtable));
v2.vec=Vector_init;
v2.vec(&v2, 1, row_v);
v1.vec=Vector_init;
v1.vec(&v1, 1, col_v);
if(row_v==col_v){
v1.matrix.add(&v1, &v2, &v3);
printf("Addition of vector:\n");
for(i=0; i<v2.matrix.row;i++){

printf("%d  ", v3.matrix.array[i]);

printf("\n");
}
}
else
printf("Addition not possible!\n");

tb_v[0]=L1_norm_v;
v1.table_v=tb_v;
v2.table_v=tb_v;
printf("%d is L1 norm of first vector \n",v1.table_v[0](&v1.matrix));
printf("%d is L1 norm of second vector \n",v2.table_v[0](&v2.matrix));

}

}

